package com.example.isabelmaria.seriestvmenu;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * clase que muestra todas las series que contienen las letras y el nombre introducido a buscar
 */
public class VerSeriesActivity extends AppCompatActivity {
    private String nombre;
    private List<SerieItem> items;
    public static Context myContext = null;
    private JSONArray series;
    //private JSONObject serie;
    private JSONObject error;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_series);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        myContext=this;

        final SharedPreferences prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
        nombre = prefs.getString("nombre", "0"); //se guarda el nombre en una variable


        /**
         * tras comprobar su hay conexión a internet, recoge el nombre guardado en el sharedprefesence en el fragment
         * de Buscar, realiza la conexión al servidor buscando por ese nombre y devuelve todas las series que
         * lo contienen.
         */
        if (isOnline()){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            StringBuilder result;

            try {
                URL url = new URL("http://isaseries.esy.es/rest/series/search/"+nombre);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                result = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

                    //JSONObject jsonCompleto = new JSONObject(result.toString());
                    items = new ArrayList<SerieItem>();
                    JSONArray series = new JSONArray(result.toString());

                    for (int n = 0; n < series.length(); n++) {
                        JSONObject serie = series.getJSONObject(n);
                        if (serie.has("error")){
                            new android.app.AlertDialog.Builder(myContext)
                                    .setTitle(getResources().getString(R.string.codigo_error))
                                    .setMessage(getResources().getString(R.string.noExiste))
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                            Intent i = new Intent(VerSeriesActivity.this, MainActivity.class);
                                            startActivity(i);
                                            finish();

                                        }
                                    })

                                    .show();
                        }else {
                            items.add(new SerieItem(serie.getString("nombre"), serie.getString("id")));
                        }
                    }
                    final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
                    recyclerView.setAdapter(new SerieAdapter(items, new RecyclerViewOnItemClickListener() {
                        @Override
                        public void onClick(View v, int position) {

                            Intent intent = new Intent(VerSeriesActivity.this, MostrarSerieActivity.class);
                            //Creamos la información a pasar entre actividades
                            Bundle b = new Bundle();
                            b.putString("id", items.get(position).getId());
                            //Añadimos la información al intent
                            intent.putExtras(b);
                            //Iniciamos la nueva actividad
                            startActivity(intent);
                            finish();

                        }
                    }));
                    recyclerView.setLayoutManager(new LinearLayoutManager(this));
                    // Mostrar el RecyclerView en vertical



            } catch (IOException e) {
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * metodo que comprueba si hay conexion a internet
     * @return
     */
    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            new android.app.AlertDialog.Builder(myContext)
                    .setTitle(getResources().getString(R.string.codigo_error))
                    .setMessage(getResources().getString(R.string.ErrorConexion))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            Toast.makeText(myContext, R.string.sinconexion, Toast.LENGTH_LONG).show();
                            Intent i = new Intent(Settings.ACTION_SETTINGS);
                            startActivity(i);
                        }
                    })

                    .show();
            return false;
        }
        return true;
    }

}
