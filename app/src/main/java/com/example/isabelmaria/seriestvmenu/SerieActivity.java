package com.example.isabelmaria.seriestvmenu;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.GatheringByteChannel;
import java.sql.Date;
import java.text.SimpleDateFormat;

public class SerieActivity extends AppCompatActivity {

    private String Promo;
    private ImageButton imageButtonPromo;
    private Animation animation;
    public static Context myContext = null;
    private String id;
    private String nombre;
    private String genero;
    private Date fecha;
    private String sinopsis;
    private String fechaConvertida;
    private String promo;
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serie);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        if(isOnline()){
            myContext = this;
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            StringBuilder result;

            try {
                Bundle bundle = this.getIntent().getExtras();
                id= bundle.getString("id");
                URL url = new URL("http://isaseries.esy.es/rest/ver/"+id);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                result = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

                final JSONObject ID = new JSONObject(result.toString());

                Serie serie= new Serie(ID.getString("id"), ID.getString("nombre"), ID.getString("genero"),ID.getString("sinopsis"), ID.getString("fechaEstreno"), ID.getString("promo"));
                nombre= ID.getString("nombre");
                genero=ID.getString("genero");
                sinopsis=ID.getString("sinopsis");

                fecha= Date.valueOf(ID.getString("fechaEstreno"));
                SimpleDateFormat sm = new SimpleDateFormat("dd/MM/yyyy");
                fechaConvertida = sm.format(fecha);

                promo=ID.getString("promo");

                LinearLayout LinearMain = (LinearLayout) findViewById(R.id.Serie);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);

                //Crear LinearLayout
                LinearLayout Imagen = new LinearLayout(new ContextThemeWrapper(this, R.style.DarkText));
                Imagen.setLayoutParams(params);
                Imagen.setOrientation(LinearLayout.HORIZONTAL);

                //Crear imagen
                ImageView imageViewNombre = new ImageView(myContext);
                serie.setData(ID.getString("imagen"));
                imageViewNombre.setImageBitmap(serie.getImagen());
                imageViewNombre.setLayoutParams(params);
                //animación que hace que la imagen aparezca lentamente
                animation = AnimationUtils.loadAnimation(myContext, R.anim.imagen);
                animation.reset();
                imageViewNombre.startAnimation(animation);

                //Añadir elementos al LinearLayout
                Imagen.addView(imageViewNombre);
                //Añadir LinearLayout al principal creado en XML
                LinearMain.addView(Imagen);

                //Crear LinearLayout
                LinearLayout contenedorNombre = new LinearLayout(new ContextThemeWrapper(this, R.style.DarkText));
                contenedorNombre.setLayoutParams(params);


                contenedorNombre.setOrientation(LinearLayout.HORIZONTAL);
                //Crear texto
                TextView textViewNombre = new TextView(new ContextThemeWrapper(this, R.style.TextReciclerView));
                textViewNombre.setLayoutParams(params);
                textViewNombre.setText(nombre);
                textViewNombre.setTextSize(30);
                //Añadir elementos al LinearLayout
                contenedorNombre.addView(textViewNombre);
                //Añadir LinearLayout al principal creado en XML
                LinearMain.addView(contenedorNombre);

                LinearLayout linearFecha = new LinearLayout(new ContextThemeWrapper(this, R.style.DarkText));
                linearFecha.setLayoutParams(params);
                linearFecha.setOrientation(LinearLayout.HORIZONTAL);

                ImageView imagenFecha = new ImageView(new ContextThemeWrapper(this, R.style.DarkText));
                imagenFecha.setImageResource(R.drawable.fecha);
                imagenFecha.setMinimumWidth(10);
                imagenFecha.setMinimumHeight(10);
                //Crear texto
                TextView textViewFecha = new TextView(new ContextThemeWrapper(this, R.style.DarkText));
                textViewFecha.setText(fechaConvertida);
                params.gravity = Gravity.CENTER;
                textViewFecha.setLayoutParams(params);
                /*imagenFecha.setPadding(8, 4, 0, 4);
                textViewFecha.setPadding(5, 10, 0, 10);*/
                textViewFecha.setTextColor(getResources().getColor(R.color.colorSplash));

                //Añadir elementos al LinearLayout
                linearFecha.addView(imagenFecha);
                linearFecha.addView(textViewFecha);
                //Añadir LinearLayout al principal creado en XML
                LinearMain.addView(linearFecha);

                //Crear LinearLayout
                LinearLayout Linerargenero = new LinearLayout(new ContextThemeWrapper(this, R.style.DarkText));
                Linerargenero.setLayoutParams(params);
                Linerargenero.setOrientation(LinearLayout.HORIZONTAL);

                //Crear texto
                TextView textViewGenero = new TextView(new ContextThemeWrapper(this, R.style.DarkText));
                textViewGenero.setText(genero);
                textViewGenero.setGravity(Gravity.CENTER);
                textViewGenero.setTextColor(getResources().getColor(R.color.colorSplash));
                //Añadir elementos al LinearLayout
                Linerargenero.addView(textViewGenero);
                //Añadir LinearLayout al principal creado en XML
                LinearMain.addView(Linerargenero);


                LinearLayout contenedorSinopsis = new LinearLayout(new ContextThemeWrapper(this, R.style.DarkText));
                contenedorSinopsis.setLayoutParams(params);
                contenedorSinopsis.setOrientation(LinearLayout.HORIZONTAL);

                //Crear texto
                WebView view= new WebView(new ContextThemeWrapper(this, R.style.DarkText));
                view.setBackgroundColor(getResources().getColor(R.color.colorView));
                sinopsis = "<html><body><p align=\"justify\">";
                sinopsis += ID.getString("sinopsis");
                sinopsis+= "</p></body></html>";
                view.loadData(sinopsis, "text/html; charset=utf-8", "UTF-8");

                //Añadir elementos al LinearLayout
                contenedorSinopsis.addView(view);
                //Añadir LinearLayout al principal creado en XML
                LinearMain.addView(contenedorSinopsis);

                LinearLayout contenedorPromo = new LinearLayout(new ContextThemeWrapper(this, R.style.DarkText));
                contenedorPromo.setLayoutParams(params);
                contenedorPromo.setOrientation(LinearLayout.HORIZONTAL);

                imageButtonPromo = new ImageButton(new ContextThemeWrapper(this, R.style.DarkText));
                imageButtonPromo.setImageResource(R.drawable.youtube);
                imageButtonPromo.setPadding(8, 4, 0, 4);
                ;

                //Promo = promo;
                //Añadir elementos al LinearLayout
                contenedorPromo.addView(imageButtonPromo);
                LinearMain.addView(contenedorPromo);
                String promo = ID.getString("promo");
                addListenerOnButtonPromo(imageButtonPromo, promo);

                /*LinearLayout LinearMain = (LinearLayout) findViewById(R.id.Serie);

                ScrollView scroll1= new ScrollView(myContext);

                LinearLayout myLinear= new LinearLayout(myContext);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                myLinear.setVerticalScrollBarEnabled(true);
                myLinear.canScrollVertically(View.SCROLL_AXIS_VERTICAL);



                //empieza el primer cardview
                CardView card1= new CardView(myContext);
                LinearLayout contenedorImagen = new LinearLayout(myContext);
                contenedorImagen.setLayoutParams(params);
                contenedorImagen.setOrientation(LinearLayout.VERTICAL);

                card1.setUseCompatPadding(true);
                card1.setPadding(1, 1, 1, 1);

                ImageView imageViewNombre = new ImageView(myContext);
                serie.setData(ID.getString("imagen"));
                imageViewNombre.setImageBitmap(serie.getImagen());
                animation = AnimationUtils.loadAnimation(myContext, R.anim.imagen);
                animation.reset();
                imageViewNombre.startAnimation(animation);

                TextView textViewNombre = new TextView(new ContextThemeWrapper(this, R.style.DarkText));

                textViewNombre.setText(nombre);
                textViewNombre.setGravity(Gravity.CENTER);
                textViewNombre.setTextColor(getResources().getColor(R.color.colorTexto));
                textViewNombre.setTextSize(20);
                //imageViewNombre.setPadding(8, 4, 0, 4);
                contenedorImagen.addView(imageViewNombre);
                contenedorImagen.addView(textViewNombre);
                card1.addView(contenedorImagen);
                LinearMain.addView(card1);

                //acaba el primer cardview

                //empieza el segundo cardview
                CardView card2= new CardView(myContext);
                LinearLayout contenedor2 = new LinearLayout(myContext);
                contenedor2.setLayoutParams(params);
                contenedor2.setOrientation(LinearLayout.VERTICAL);

                card2.setUseCompatPadding(true);
                card2.setPadding(1, 1, 1, 1);

                TextView textViewGenero = new TextView(new ContextThemeWrapper(this, R.style.DarkText));
                textViewGenero.setText(genero);
                textViewGenero.setGravity(Gravity.CENTER);
                textViewGenero.setTextColor(getResources().getColor(R.color.colorTexto));

                TextView textViewFecha = new TextView(new ContextThemeWrapper(this, R.style.DarkText));
                textViewFecha.setText(fechaConvertida);
                textViewFecha.setGravity(Gravity.CENTER);
                textViewFecha.setTextColor(getResources().getColor(R.color.colorTexto));
                //imageViewNombre.setPadding(8, 4, 0, 4);
                contenedor2.addView(textViewGenero);
                contenedor2.addView(textViewFecha);
                card2.addView(contenedor2);
                LinearMain.addView(card2);

                //acaba el segundo cardview

                //empieza el tercer cardview
                CardView card3= new CardView(myContext);
                LinearLayout contenedor3 = new LinearLayout(myContext);
                contenedor3.setLayoutParams(params);
                contenedor3.setOrientation(LinearLayout.VERTICAL);

                card3.setUseCompatPadding(true);
                card3.setPadding(1, 1, 1, 1);

                WebView view= new WebView(new ContextThemeWrapper(this, R.style.DarkText));
                sinopsis = "<html><body><p align=\"justify\">";
                sinopsis+= ID.getString("sinopsis");
                sinopsis+= "</p></body></html>";
                view.loadData(sinopsis, "text/html; charset=utf-8", "UTF-8");
                //TextView textViewSinopsis = new TextView(new ContextThemeWrapper(this, R.style.DarkText));
                //textViewSinopsis.setText(sinopsis);
                //textViewSinopsis.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                //imageViewNombre.setPadding(8, 4, 0, 4);
                //contenedor3.addView(textViewSinopsis);
                //contenedor3.addView(view);
                card3.addView(view);
                LinearMain.addView(card3);
                //acaba el tercer cardview


                LinearLayout contenedorPromo = new LinearLayout(new ContextThemeWrapper(this, R.style.DarkText));
                contenedorPromo.setLayoutParams(params);
                contenedorPromo.setOrientation(LinearLayout.HORIZONTAL);
                //Crear imagen
                imageButtonPromo = new ImageButton(new ContextThemeWrapper(this, R.style.DarkText));
                imageButtonPromo.setImageResource(R.drawable.youtube);
                imageButtonPromo.setPadding(8, 4, 0, 4);;

                //Promo = promo;
                //Añadir elementos al LinearLayout
                contenedorPromo.addView(imageButtonPromo);
                LinearMain.addView(contenedorPromo);
                String promo = ID.getString("promo");
                addListenerOnButtonPromo(imageButtonPromo, promo);

                scroll1.addView(myLinear);
                LinearMain.addView(scroll1);*/

                //Añadir demas campos antes del scroll1

            /*LinearLayout contenedorNombre = new LinearLayout(new ContextThemeWrapper(this, R.style.DarkText));
            contenedorNombre.setLayoutParams(params);
            contenedorNombre.setOrientation(LinearLayout.HORIZONTAL);
            TextView textViewNombre = new TextView(new ContextThemeWrapper(this, R.style.DarkText));
            textViewNombre.setText(ID.getString("nombre"));
            textViewNombre.setPadding(5, 10, 0, 10);
            textViewNombre.setLayoutParams(params);
            contenedorNombre.addView(textViewNombre);
            LinearMain.addView(contenedorNombre);*/

            /*LinearLayout contenedorGenero = new LinearLayout(new ContextThemeWrapper(this, R.style.DarkText));
            contenedorGenero.setLayoutParams(params);
            contenedorGenero.setOrientation(LinearLayout.HORIZONTAL);
            TextView textViewGenero = new TextView(new ContextThemeWrapper(this, R.style.DarkText));
            textViewGenero.setText(ID.getString("genero"));
            textViewGenero.setPadding(5, 10, 0, 10);
            textViewGenero.setLayoutParams(params);
            contenedorGenero.addView(textViewGenero);
            LinearMain.addView(contenedorGenero);

            LinearLayout contenedorSinopsis = new LinearLayout(new ContextThemeWrapper(this, R.style.DarkText));
            contenedorSinopsis.setLayoutParams(params);
            contenedorSinopsis.setOrientation(LinearLayout.HORIZONTAL);
            TextView textViewSinopsis = new TextView(new ContextThemeWrapper(this, R.style.DarkText));
            textViewSinopsis.setText(ID.getString("sinopsis"));
            textViewSinopsis.setPadding(5, 10, 0, 10);
            textViewSinopsis.setLayoutParams(params);
            contenedorSinopsis.addView(textViewSinopsis);
            LinearMain.addView(contenedorSinopsis);*/

            /*LinearLayout contenedorFecha = new LinearLayout(new ContextThemeWrapper(this, R.style.DarkText));
            contenedorFecha.setLayoutParams(params);
            contenedorFecha.setOrientation(LinearLayout.HORIZONTAL);
            TextView textViewFecha = new TextView(new ContextThemeWrapper(this, R.style.DarkText));
            textViewFecha.setText(ID.getString("fechaEstreno"));
            textViewFecha.setPadding(5, 10, 0, 10);
            textViewFecha.setLayoutParams(params);
            contenedorFecha.addView(textViewFecha);
            LinearMain.addView(contenedorFecha);*/

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
          //  addListenerOnButton();
        }

    }

    public void onBackPressed() {
        Intent intent = new Intent(SerieActivity.this, VerSeriesActivity.class);
        startActivity(intent);
        finish();
    }

    public void addListenerOnButtonPromo( ImageButton imageButtonTelefono , final String promo) {
        imageButtonTelefono.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Bundle b = new Bundle();
                b.getString("promo");
                b.putString("promo", promo);
                b.putString("id", id);

                if (promo.length()<4){
                    Toast.makeText(myContext, R.string.url, Toast.LENGTH_SHORT).show();
                }else {
                    if (promo.substring(0, 4).equalsIgnoreCase("http")) {
                        /*Intent intent = new Intent(SerieActivity.this, WebActivity.class);
                        intent.putExtras(b);
                        startActivity(intent);*/
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(promo)));
                        finish();

                    } else {
                        new AlertDialog.Builder(myContext)
                                .setTitle(getResources().getString(R.string.codigo_error))
                                .setMessage(getResources().getString(R.string.FalloURL))
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                    }
                                })
                                .show();
                    }
                }

            }
        });
    }

    public void addListenerOnButton() {

        //usando el metodo Bundle pasamos la direccion web, de twitter y de facebook a la actividad donde vamos a abrirla.
        //se comprueba si la direccion es correcta, mirando su empieza por HTTP o HTTPS, si no es así mostrara un mensaje avisando
        //De que la direccion a la que se quiere acceder es incorrecta
        imageButtonPromo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Bundle b = new Bundle();
                b.getString("promo");
                b.putString("promo", Promo);
                b.putString("id", id);

                if (Promo.substring(0, 4).equalsIgnoreCase("http")) {

                    Intent intent = new Intent(SerieActivity.this, WebActivity.class);
                    intent.putExtras(b);
                    startActivity(intent);
                    finish();

                } else {
                    new AlertDialog.Builder(myContext)
                            .setTitle(getResources().getString(R.string.codigo_error))
                            .setMessage(getResources().getString(R.string.FalloURL))
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .show();
                }


            }

        });

    }

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            new AlertDialog.Builder(myContext)
                    .setTitle(getResources().getString(R.string.codigo_error))
                    .setMessage(getResources().getString(R.string.ErrorConexion))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            Toast.makeText(myContext, R.string.sinconexion, Toast.LENGTH_LONG).show();
                            Intent i = new Intent(Settings.ACTION_SETTINGS);
                            startActivity(i);
                        }
                    })

                    .show();
            return false;
        }
        return true;
    }

}
