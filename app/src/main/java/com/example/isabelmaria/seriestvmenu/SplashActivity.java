package com.example.isabelmaria.seriestvmenu;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Window;

import java.util.Timer;
import java.util.TimerTask;

import pl.droidsonroids.gif.GifImageView;

public class SplashActivity extends Activity {

    // Set the duration of the splash screen
    private GifImageView splash;
    // Set the duration of the splash screen
    private static final long SPLASH_SCREEN_DELAY = 2300;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //addShortcut();
        // Set portrait orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Hide title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        splash= (GifImageView) findViewById(R.id.actividadSplash);
        splash.setImageResource(R.drawable.claqueta_gif);

        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                Intent intent = new Intent().setClass(
                        SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        };

        // Simulate a long loading process on application startup.
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_SCREEN_DELAY);
    }
}
