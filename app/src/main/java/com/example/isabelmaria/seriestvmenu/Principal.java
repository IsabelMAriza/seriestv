package com.example.isabelmaria.seriestvmenu;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by isabelmaria on 11/05/2016.
 */

/**
 * clase que muestra todas las series disponibles
 */
public class Principal extends Fragment {

    private EditText txtNuevasSeries;
    public static final String DEBUG_TAG= "PrincipalActivity";
    public static Context myContext= null;
    ListViewAdapter adapter;
    ArrayList<String> id = new ArrayList<>();
    ArrayList<String> imagenes = new ArrayList<>();
    ArrayList<String> nombre = new ArrayList<>();


    View myView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.principal, container, false);
        //myContext=this;


        final ListView lista = (ListView) myView.findViewById(R.id.listViewPrincipal);
        adapter = new ListViewAdapter(getActivity(), id, imagenes, nombre);
        lista.setAdapter(adapter);

        /**
         * recoge el id de la serie señalada y abre la actividad MostrarSerieActivity donde muestra sus datos
         */
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView adapterView, View view, int i, long l) {
                //Toast.makeText(getApplicationContext(), "presiono " + i, Toast.LENGTH_SHORT).show();
                Bundle bundle = new Bundle();
                bundle.putString("id", id.get(i));
                Intent intent = new Intent(getActivity(), MostrarSerieActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        StringBuilder result = new StringBuilder();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        HttpURLConnection urlConnection;


        /**
         * realiza una conexión con el servidor y devuelve todas las series disponibles, se pasan a un JSONArray y se
         * añader a su ArrayList correspondiente.
         */
        try {
            String direccion = "http://isaseries.esy.es/rest/ver";

            URL url = new URL(direccion);


            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
                //MyLog.i("entrada json", line);
            }

            final JSONArray series = new JSONArray(result.toString());
            for (int n=0; n<series.length();n++){
                JSONObject serie= series.getJSONObject(n);
                id.add(serie.getString("id"));
                imagenes.add(serie.getString("imagen"));
                nombre.add(serie.getString("nombre"));
            }



        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return myView;


    }

}
