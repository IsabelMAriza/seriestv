package com.example.isabelmaria.seriestvmenu;

/**
 * Created by isabelmaria on 22/03/2016.
 */
public class SerieItem {
    private String nombre;
    private String id;

    public SerieItem(String nombre, String id) {
        this.nombre = nombre;
        this.id=id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
