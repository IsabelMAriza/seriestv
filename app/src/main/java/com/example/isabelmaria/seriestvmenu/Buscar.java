package com.example.isabelmaria.seriestvmenu;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by isabelmaria on 22/04/2016.
 */

/**
 * clase para buscar el nombre de una serie concreta
 */
public class Buscar extends Fragment {

    private EditText txtNuevasSeries;

    View myView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.buscar, container, false);
        //myContext=this;

        txtNuevasSeries= (EditText) myView.findViewById(R.id.editTextSerie);

        /**
         * recoge el nombre de la serie introducido y lo guarda en un sharedpreference para poder acceder a el desde la
         * siguiente actividad.
         */
        myView.findViewById(R.id.buttonSerie).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(txtNuevasSeries.getText())) {//comprueba que el campo no este vacio
                }else {
                    if (isOnline()){//llama al metodo comprobar la conexión
                        SharedPreferences prefs = getActivity().getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("nombre", txtNuevasSeries.getText().toString());
                        editor.commit();

                       Intent intent = new Intent(getActivity(), VerSeriesActivity.class);
                        startActivity(intent);
                    }
                }

            }
        });

        return myView;


    }

    /**
     * método que comprueba si hay conexión a internet
     * @return
     */
    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            new android.app.AlertDialog.Builder(getActivity())
                    .setTitle(getResources().getString(R.string.codigo_error))
                    .setMessage(getResources().getString(R.string.ErrorConexion))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            Toast.makeText(getActivity(), R.string.sinconexion, Toast.LENGTH_LONG).show();
                            Intent i = new Intent(Settings.ACTION_SETTINGS);
                            startActivity(i);
                        }
                    })

                    .show();
            return false;
        }
        return true;
    }
}
