package com.example.isabelmaria.seriestvmenu;

import android.view.View;

/**
 * Created by isabelmaria on 22/03/2016.
 */
public interface RecyclerViewOnItemClickListener {
    void onClick(View i, int position);
}
