package com.example.isabelmaria.seriestvmenu;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by isabelmaria on 22/04/2016.
 */

/**
 * Clase usada para añadir una nueva serie sobre la base de datos
 */
public class Anadir extends Fragment {

    private ImageView verImagen;

    private ProgressDialog pDialog;
    private JSONObject json;
    //Initialize webservice URL
    private int success = 0;

    public static final String UPLOAD_URL = "http://isaseries.esy.es/rest/upload.php";
    public static final String UPLOAD_KEY = "imagen";
    public static final String UPLOAD_NOMBRE = "nombre";
    public static final String UPLOAD_GENERO = "genero";
    public static final String UPLOAD_SINOPSIS = "sinopsis";
    public static final String UPLOAD_FECHAESTRENO = "fechaEstreno";
    public static final String UPLOAD_PROMO = "promo";
    private Uri filePath;

    private EditText nombre;
    private EditText fechaEstreno;
    private EditText promo;
    private EditText sinopsis;

    private Spinner spinner;

    private String nombreSerie;
    private String fechaEstrenoSerie;
    private String fechaConvertida;
    private String generoSerie;
    private String promoSerie;
    private String sinopsisSerie;

    /*private String[] valores = {"Acción",
            "Animación",
            "Anime",
            "Aventura",
            "Ciencia Ficción",
            "Comedia",
            "Crimen",
            "Deportes",
            "Documental",
            "Drama",
            "Fantasía",
            "Infantil",
            "Misterio",
            "Terror"};*/

    private Bitmap imagenGuardada;

    private int PICK_IMAGE_REQUEST = 1;

    View myView;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.anadir, container, false);



        verImagen = (ImageView) myView.findViewById(R.id.imageViewImagen);

        nombre = (EditText) myView.findViewById(R.id.editTextNombre);
        fechaEstreno = (EditText) myView.findViewById(R.id.editTextfechaEstreno);

        spinner = (Spinner) myView.findViewById(R.id.spinnerGenero);
       // spinner.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, valores));
        ArrayAdapter<CharSequence> generos= ArrayAdapter.createFromResource(getActivity(), R.array.generos,android.R.layout.simple_spinner_item);
        generos.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(generos);

        promo = (EditText) myView.findViewById(R.id.editTextPromo);
        sinopsis = (EditText) myView.findViewById(R.id.editTextSinopsis);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                generoSerie = (String) adapterView.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // vacio

            }
        });

        /**
         * llama al metodo elegir fecha
         */
        myView.findViewById(R.id.btnChangeDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        /**
         * llama al metodo elegir imagen
         */
        myView.findViewById(R.id.buttonImagen).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        /**
         * recoge los datos introducidos por el usuario y llama al metodo subir datos.
         */
        myView.findViewById(R.id.buttonAceptar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline()) {
                    Log.e("Nombre: ", nombre.getText().toString());
                    nombreSerie = nombre.getText().toString();
                    fechaEstrenoSerie = fechaEstreno.getText().toString();
                    sinopsisSerie = sinopsis.getText().toString();
                    promoSerie = promo.getText().toString();

                    //comprueba que los campos de texto no esten vacíos
                    if (TextUtils.isEmpty(nombreSerie) | TextUtils.isEmpty(fechaEstrenoSerie) | TextUtils.isEmpty(generoSerie) | TextUtils.isEmpty(sinopsisSerie) | TextUtils.isEmpty(promoSerie)) {
                        if (TextUtils.isEmpty(nombreSerie)){
                            nombre.setError(getString(R.string.error_field_required));
                        }else {
                            if (TextUtils.isEmpty(fechaEstrenoSerie)){
                                fechaEstreno.setError(getString(R.string.error_field_required));
                            }else{
                                if (TextUtils.isEmpty(sinopsisSerie)){
                                    sinopsis.setError(getString(R.string.error_field_required));
                                } else {
                                    if (TextUtils.isEmpty(promoSerie)){
                                        promo.setError(getString(R.string.error_field_required));
                                    }
                                }
                            }
                        }
                    } else {
                        if (null == verImagen.getDrawable()) {//comprueba que se haya seleccionado una imagen
                            Toast.makeText(getActivity(), R.string.seleccionarimagen, Toast.LENGTH_SHORT).show();
                        } else {
                            try { //comprueba formato fecha
                                String[] asFecha = fechaEstrenoSerie.split("-");
                                fechaConvertida = asFecha[2] + "-" + asFecha[1] + "-" + asFecha[0];
                                uploadImage();

                            } catch (Exception e) {
                                Toast.makeText(getActivity(), R.string.fechaincorrecta, Toast.LENGTH_SHORT).show();
                            }


                        }
                    }
                }


            }
        });
        return myView;
    }

    /**
     * método que abre la galeria del movil
     */
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    /**
     * recoge la imagen selecionada en la galeria y la transforma en bitmap para mostrarla por pantalla
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && data != null && data.getData() != null) {

            filePath = data.getData();
            try {
                imagenGuardada = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                verImagen.setImageBitmap(imagenGuardada);

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                imagenGuardada.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * codifica la imagen recogida en base64
     * @param bmp
     * @return
     */
    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    /**
     * metodo que contiene una clase interna usaba para recoger todos los datos introducidos por el usuario, realiza
     * una conexión con un archivo en php alojado en un servidor e inserta los datos en la base de datos
     */
    private void uploadImage() {
        class UploadImage extends AsyncTask<Bitmap, Void, String> {

            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(), "Uploading", "Please wait...", true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.correctamente), Toast.LENGTH_LONG).show();
                Fragment myFragment=null;

                myFragment= new Principal();
                FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, myFragment)
                        .commit();
            }

            @Override
            protected String doInBackground(Bitmap... params) {
                Bitmap bitmap = params[0];
                String uploadImage = getStringImage(bitmap);

                HashMap<String, String> data = new HashMap<>();
                data.put(UPLOAD_KEY, uploadImage);
                data.put(UPLOAD_NOMBRE, nombreSerie);
                data.put(UPLOAD_GENERO, generoSerie);
                data.put(UPLOAD_SINOPSIS, sinopsisSerie);
                data.put(UPLOAD_FECHAESTRENO, fechaConvertida);
                data.put(UPLOAD_PROMO, promoSerie);

                String result = rh.sendPostRequest(UPLOAD_URL, data);

                return result;
            }
        }

        UploadImage ui = new UploadImage();
        ui.execute(imagenGuardada);
    }


    /**
     * método que comprueba si hay conexión a internet
     * @return
     */
    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            new android.app.AlertDialog.Builder(getActivity())
                    .setTitle(getResources().getString(R.string.codigo_error))
                    .setMessage(getResources().getString(R.string.ErrorConexion))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            Toast.makeText(getActivity(), R.string.sinconexion, Toast.LENGTH_LONG).show();
                            Intent i = new Intent(Settings.ACTION_SETTINGS);
                            startActivity(i);
                        }
                    })

                    .show();
            return false;
        }
        return true;
    }


    /**
     * abre un calendario para poder seleccionar la fecha que se desee
     */
    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            fechaEstreno.setText(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear+1)
                    + "-" + String.valueOf(year));
        }
    };

}

