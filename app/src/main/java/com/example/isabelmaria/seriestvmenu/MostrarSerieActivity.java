package com.example.isabelmaria.seriestvmenu;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * clase que muestra todos los datos de una serie concreta
 */
public class MostrarSerieActivity extends AppCompatActivity {

    private String Promo;
    private ImageButton imageButtonPromo;
    private Animation animation;
    public static Context myContext = null;
    private String id;
    private String nombre;
    private String genero;
    private Date fecha;
    private String sinopsis;
    private String fechaConvertida;
    private String promo;

    private ImageView imagen;
    private TextView nombreSerie;
    private TextView fechaSerie;
    private ImageView fechaLogo;
    private TextView generoSerie;
    private WebView sinopsisSerie;
    private ImageButton promoSerie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_serie);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imagen= (ImageView) findViewById(R.id.imageViewImagen);
        nombreSerie= (TextView) findViewById(R.id.textViewNombre);
        fechaSerie= (TextView) findViewById(R.id.textViewFechaEstreno);
        fechaLogo= (ImageView) findViewById(R.id.imageViewFechaLogo);
        generoSerie= (TextView) findViewById(R.id.textViewGenero);
        sinopsisSerie= (WebView) findViewById(R.id.webViewSinopsis);
        promoSerie = (ImageButton) findViewById(R.id.imageButtonPromo);

        if (isOnline()){//comprueba si hay conexión

            myContext = this;
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            StringBuilder result;

        /**
         * realiza la conexión con el servidor y devuelve todos los datos que son recogidos en un JSON. Luego se muestran
         * por pantalla.
         */
            try {
                Bundle bundle = this.getIntent().getExtras();
                id= bundle.getString("id");
                URL url = new URL("http://isaseries.esy.es/rest/ver/"+id);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                result = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

                final JSONObject ID = new JSONObject(result.toString());

                Serie serie= new Serie(ID.getString("id"), ID.getString("nombre"), ID.getString("genero"),ID.getString("sinopsis"), ID.getString("fechaEstreno"), ID.getString("promo"));
                nombre= ID.getString("nombre");
                genero=ID.getString("genero");
                sinopsis=ID.getString("sinopsis");
                fecha= Date.valueOf(ID.getString("fechaEstreno"));
                SimpleDateFormat sm = new SimpleDateFormat("dd/MM/yyyy");
                fechaConvertida = sm.format(fecha);
                promo=ID.getString("promo");

                serie.setData(ID.getString("imagen"));
                imagen.setImageBitmap(serie.getImagen());

                //animación que hace que la imagen vaya apareciendo poco a poco
                animation = AnimationUtils.loadAnimation(myContext, R.anim.imagen);
                animation.reset();
                imagen.startAnimation(animation);

                nombreSerie.setText(nombre);
                fechaSerie.setText(fechaConvertida);
                generoSerie.setText(genero);
                /*sinopsis = "<html><body><p align=\"justify\">";
                sinopsis += ID.getString("sinopsis");
                sinopsis+= "</p></body></html>";*/
                String text = "<html><head>"
                        + "<style type=\"text/css\">body{color: #CFD6D7; text-align:justify; background-color: 0x00000000}"
                        + "</style></head>"
                        + "<body>"
                        + sinopsis
                        + "</body></html>";
                sinopsisSerie.setBackgroundColor(Color.TRANSPARENT);
                sinopsisSerie.loadData(text, "text/html; charset=utf-8", "UTF-8");

                //recoge la url de la promo de la serie indicada y abre la aplicacion de youtube o el navegador mostrando
                //el video correspondiente.
                promoSerie.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle b = new Bundle();
                        b.getString("promo");
                        b.putString("promo", promo);
                        b.putString("id", id);

                        if (promo.length() < 4) {//comprueba que la url tenga mas de 4 letras
                            Toast.makeText(myContext, R.string.url, Toast.LENGTH_SHORT).show();
                        } else {
                            if (promo.substring(0, 4).equalsIgnoreCase("http")) { //comprueba que la url empieza por http
                        /*Intent intent = new Intent(MostrarSerieActivity.this, WebActivity.class);
                        intent.putExtras(b);
                        startActivity(intent);*/
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(promo)));
                                finish();

                            } else {
                                new AlertDialog.Builder(myContext)
                                        .setTitle(getResources().getString(R.string.codigo_error))
                                        .setMessage(getResources().getString(R.string.FalloURL))
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // continue with delete
                                            }
                                        })
                                        .show();
                            }
                        }
                    }
                });



            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * método que comprueba si hay conexión a internet
     * @return
     */
    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            new android.app.AlertDialog.Builder(myContext)
                    .setTitle(getResources().getString(R.string.codigo_error))
                    .setMessage(getResources().getString(R.string.ErrorConexion))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            Toast.makeText(myContext, R.string.sinconexion, Toast.LENGTH_LONG).show();
                            Intent i = new Intent(Settings.ACTION_SETTINGS);
                            startActivity(i);
                        }
                    })

                    .show();
            return false;
        }
        return true;
    }

}
