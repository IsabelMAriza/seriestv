package com.example.isabelmaria.seriestvmenu;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by isabelmaria on 11/05/2016.
 */

/**
 * clase que rellena un list view con los nombres e imagenes de todas la series disponibles
 */
public class ListViewAdapter extends BaseAdapter {
    // Declare Variables
    Context context;
    ArrayList<String> id = new ArrayList<>();
    ArrayList<String> imagenes = new ArrayList<>();
    ArrayList<String> nombre = new ArrayList<>();
    LayoutInflater inflater;

    private Bitmap bitmap;

    public ListViewAdapter(Context context, ArrayList<String> titulos, ArrayList<String> imagenes, ArrayList<String> posicion) {
        this.context = context;
        this.id = titulos;
        this.imagenes = imagenes;
        this.nombre = posicion;
    }

    @Override
    public int getCount() {
        return id.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        // Declare Variables
        TextView txtTitle;
        ImageView imgImg;

        //http://developer.android.com/intl/es/reference/android/view/LayoutInflater.html
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.main_row, parent, false);

        // Locate the TextViews in listview_item.xml
        txtTitle = (TextView) itemView.findViewById(R.id.list_row_title);
        imgImg = (ImageView) itemView.findViewById(R.id.list_row_image);
        Serie serie= new Serie(id.get(position), nombre.get(position));
        // Capture position and set to the TextViews
        txtTitle.setText(nombre.get(position));

        serie.setData(imagenes.get(position));
        imgImg.setImageBitmap(serie.getImagen());


        return itemView;
    }
}
