package com.example.isabelmaria.seriestvmenu;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

/**
 * Created by isabelmaria on 28/03/2016.
 */
public class Serie {
    private String id;
    private String nombre;
    private String genero;
    private String sinopsis;
    private String fechaEstreno;
    private String promo;
    private Bitmap imagen;
    private String data;

    public Serie(String id, String name, String genero, String sinopsis, String fechaEstreno, String promo) {
        this.id = id;
        this.nombre = name;
        this.genero= genero;
        this.sinopsis= sinopsis;
        this.fechaEstreno=fechaEstreno;
        this.promo= promo;

    }

    public Serie(String id, String name) {
        this.id = id;
        this.nombre = name;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return nombre;
    }

    public void setName(String name) {
        this.nombre = name;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(String fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }


    public String getPromo() {
        return promo;
    }

    public void setPromo(String promo) {
        this.promo = promo;
    }

    public String getData() {
        return data;
    }

    /**
     * recupera la imagen enviada desde la base de datos y la codifica en base64
     * @param data
     */
    public void setData(String data) {
        this.data = data;
        try {
            byte[] byteData = Base64.decode(data, Base64.DEFAULT);
            this.imagen = BitmapFactory.decodeByteArray(byteData, 0,
                    byteData.length);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap getImagen() {
        return imagen;
    }
}
