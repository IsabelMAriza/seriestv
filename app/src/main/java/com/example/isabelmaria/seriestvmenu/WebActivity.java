package com.example.isabelmaria.seriestvmenu;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class WebActivity extends AppCompatActivity {

    public static final String DEBUG_TAG= "WebActivity";

    public static Context myContext= null;
    String id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Localizar los controles

        //Recuperamos la información pasada en el intent
        myContext=this;
        if (isOnline()) { //comprobamos si hay conexion a internet

            //segun desde donde se accesa a esta actividad, ya sea desde un acontecimiento o un evento, y sea desde twitter, facebook o
            //web abrira un navegador interno mostrando la informacion que queremos ver.
            Bundle bundle = this.getIntent().getExtras();
            String urlPromo= bundle.getString("promo");
            id= bundle.getString("id");

            //Construimos el mensaje a mostrar
            WebView myWebView = (WebView) this.findViewById(R.id.webViewWeb);
            // Enable JavaScript
            WebSettings webSettings = myWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            // Bind a new interface between your JavaScript and Android code
            myWebView.addJavascriptInterface(new WebAppInterface(this), "Android");
            // Provide a WebViewClient for your WebView
            myWebView.setWebViewClient(new WebViewClient());

            myWebView.loadUrl(urlPromo);

        }

    }

    public boolean isOnline() { //metodo que comprueba la conexion, si esta desactivado muestra un mensaje de aviso
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            new AlertDialog.Builder(myContext)
                    .setTitle(getResources().getString(R.string.codigo_error))
                    .setMessage(getResources().getString(R.string.ErrorConexion))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            Toast.makeText(myContext, R.string.sinconexion, Toast.LENGTH_LONG).show();
                            Intent i = new Intent(Settings.ACTION_SETTINGS);
                            startActivity(i);
                        }
                    })

                    .show();
            return false;
        }
        return true;


    }

    public void onBackPressed() {
        Intent intent = new Intent(WebActivity.this, SerieActivity.class);
        Bundle b = new Bundle();
        b.putString("id", id);
        //Añadimos la información al intent
        intent.putExtras(b);
        //Iniciamos la nueva actividad
        startActivity(intent);
        finish();

    }

}
