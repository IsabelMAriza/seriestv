package com.example.isabelmaria.seriestvmenu;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by isabelmaria on 22/03/2016.
 */
public class SerieAdapter extends RecyclerView.Adapter<SerieAdapter.AcontecimientoViewHolder> {

    public static final String DEBUG_TAG= "AcontecimientoAdapter";
    private List<SerieItem> items;
    private RecyclerViewOnItemClickListener recyclerViewOnItemClickListener;

    public SerieAdapter(@NonNull List<SerieItem> items, RecyclerViewOnItemClickListener recyclerViewOnItemClickListener) {
        this.items = items;
        this.recyclerViewOnItemClickListener = recyclerViewOnItemClickListener;
    }

    @Override
    public AcontecimientoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        AcontecimientoViewHolder avh = new AcontecimientoViewHolder(row, recyclerViewOnItemClickListener);
        return avh;
    }

    @Override
    public void onBindViewHolder(AcontecimientoViewHolder holder, int position) {
        SerieItem item = items.get(position);
        holder.getNombreTextView().setText(item.getNombre());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    //Se implementa el ViewHolder que se ha utilizado anteriormente
    public static class AcontecimientoViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{
        private TextView nombreTextView;
        private RecyclerViewOnItemClickListener recyclerViewOnItemClickListener;

        public AcontecimientoViewHolder(View itemView, RecyclerViewOnItemClickListener recyclerViewOnItemClickListener) {
            super(itemView);
            nombreTextView = (TextView) itemView.findViewById(R.id.nombreTextView);
            this.recyclerViewOnItemClickListener = recyclerViewOnItemClickListener;
            itemView.setOnClickListener(this);
        }

        public TextView getNombreTextView() {
            return nombreTextView;
        }

        @Override
        public void onClick(View v) {
            recyclerViewOnItemClickListener.onClick(v, getAdapterPosition());
        }
    }
}
