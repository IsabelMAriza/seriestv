package com.example.isabelmaria.seriestvmenu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by isabelmaria on 18/05/2016.
 */

/**
 * clase que se carga cuando la aplicación se abre sin tener acceso a internat. Da la posibilidad de ir directamente
 * a ajustes y activarlo.
 */
public class SinConexion extends Fragment {

    private BroadcastReceiver receiver;

    View myView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.sin_conexion, container, false);

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        filter.addAction("android.net.wifi.STATE_CHANGE");
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");


        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //do something based on the intent's action
                ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

                boolean isConnected = wifi != null && wifi.isConnectedOrConnecting() || mobile != null && mobile.isConnectedOrConnecting();
                if (isConnected) {

                    /*View coordinatorLayout =myView.findViewById(R.id.activity_sinConexion);
                    Snackbar snackbar= Snackbar.make(coordinatorLayout, getResources().getString(R.string.conectado), Snackbar.LENGTH_INDEFINITE);
                    snackbar.show();*/
                }else{
                    View coordinatorLayout =myView.findViewById(R.id.activity_sinConexion);
                    Snackbar.make(coordinatorLayout, getResources().getString(R.string.desconectado), Snackbar.LENGTH_INDEFINITE)
                            .setAction("Datos movil", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent i = new Intent(Settings.ACTION_SETTINGS);
                                    startActivity(i);
                                }
                            }).show();

                }
            }
        };
        getActivity().registerReceiver(receiver, filter);

        return myView;
    }
}
